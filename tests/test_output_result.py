import pytest

from pyatran.output import Result

from .test_read_sciatran import mk_testdata_path


def test_resultclass_deprecationwarning():
    p = mk_testdata_path('testdata_int_3.5.2.zip')
    with pytest.warns(DeprecationWarning):
        Result(p)
