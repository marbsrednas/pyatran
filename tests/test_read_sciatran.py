import glob
import os.path
import re
import sys
import warnings

import pytest

import numpy as np
from numpy.testing import assert_array_equal

from .context import pyatran

from pyatran.output import read_sciatran_output


def mk_testdata_path(fn):
    return os.path.join(os.path.dirname(__file__), 'data', fn)


FNS_TESTDATA = [fn for fn in glob.glob(mk_testdata_path('testdata_*'))]


# from https://docs.pytest.org/en/latest/example/parametrize.html#a-quick-port-of-testscenarios  # noqa
def pytest_generate_tests(metafunc):
    idlist = []
    argvalues = []
    if not metafunc.cls.scenarios:  # no scenarios defined - missing data?
        argnames = []
        warnings.warn(
            "No test data available: skipping read_sciatran_output tests.",
            UserWarning)
    for scenario in metafunc.cls.scenarios:
        idlist.append(scenario[0])
        items = scenario[1].items()
        argnames = [x[0] for x in items]
        argvalues.append(([x[1] for x in items]))
    metafunc.parametrize(argnames, argvalues, ids=idlist, scope="class")


class TestReadSciatranOutput(object):
    scenarios = [(os.path.split(p)[1], {'path': p}) for p in FNS_TESTDATA]

    def test_add_version_to_output(self, path):
        fn = os.path.split(path)[1]

        # mark failing tests
        if os.path.splitext(fn)[1].lower().endswith('xz'):
            if sys.version_info < (3, 3):
                pytest.skip(".xz archives are not supported on Python < 3.3")

        # regex from https://stackoverflow.com/a/4703409/152439
        try:
            all_versions = re.findall(r"[-+]?\d*\.\d+.\d+", fn)
            assert len(all_versions) == 1  # something's wrong with this fn
            scia_version = all_versions[0]
        except AssertionError:
            all_versions = re.findall(r"[-+]?\d*\.\d+", fn)
            assert len(all_versions) == 1  # something's wrong with this fn
            scia_version = all_versions[0]
        res = read_sciatran_output(path, stokes_dim=False)
        assert res.attrs.get('SCIATRAN_version') == scia_version
        assert res.attrs.get('pyatran_version') == pyatran.__version__

    def test_reshape_stokes(self, path):
        if os.path.split(path)[1] not in [
                'testdata_int_domv-stokes_3.8.5.tar.bz2',
                'testdata_raman_domv-stokes_3.8.5.tar.bz2']:
            return

        res = read_sciatran_output(path, stokes_dim=True)
        res_orig = read_sciatran_output(path, stokes_dim=False)
        assert_array_equal(res_orig['stokes_component'].values,
                           np.tile(np.arange(1, 5), 3))

        # testing output_map reshape
        VARS_OUTPUT_MAP = [
            'sza', 'vza', 'raa', 'tangent_height_geom', 'tangent_height_refr',
            'sza_refr', 'ss_angle', 'output_altitude']
        for var in VARS_OUTPUT_MAP:
            assert res_orig[var].size % 4 == 0
            assert res_orig[var].size % res[var].size == 0
            assert res_orig[var].size // res[var].size == 4

        # test new stokes_component
        assert res['stokes_component'].values.tolist() == ['I', 'Q', 'U', 'V']

        # test arrays
        ix1 = res_orig['stokes_component'] == 1
        ix2 = res_orig['stokes_component'] == 2
        ix3 = res_orig['stokes_component'] == 3
        ix4 = res_orig['stokes_component'] == 4
        for ix, sc in zip([ix1, ix2, ix3, ix4], ['I', 'Q', 'U', 'V']):
            assert_array_equal(
                res['intensity'].loc[:, sc].values,
                res_orig['intensity'][ix].values)

    def test_absorber_list(self, path):
        if os.path.split(path)[1] not in [
                'testdata_wf_3.3.2.zip']:
            return

        res_all = read_sciatran_output(path)
        actual = [v for v in res_all.data_vars if v.startswith('wf_')]
        required = ['wf_no2', 'wf_no3']
        assert set(actual) == set(required)

        res_all_explicit = read_sciatran_output(path, ['no3', 'no2'])
        actual = [v for v in res_all_explicit.data_vars if v.startswith('wf_')]
        required = ['wf_no2', 'wf_no3']
        assert set(actual) == set(required)

        res_all_onelist = read_sciatran_output(path, ['no2'])
        actual = [v for v in res_all_onelist.data_vars if v.startswith('wf_')]
        required = ['wf_no2']
        assert set(actual) == set(required)

        res_all_str = read_sciatran_output(path, 'no2')
        actual = [v for v in res_all_str.data_vars if v.startswith('wf_')]
        required = ['wf_no2']
        assert set(actual) == set(required)
