from __future__ import print_function, division, unicode_literals

from ._version import get_versions
__version__ = get_versions()['version']
del get_versions
