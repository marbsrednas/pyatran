# -*- coding: utf-8 -*-

"""pyatran.util
===============

Utilities for internal use within pyatran

.. autosummary::
   :toctree: api/

   mkbasedir

"""


from .util import mkbasedir
